import { receiveCard } from "./functions/sendRequest.js";
import Modal from "./modules/modal.js";
import UserForm from './modules/userForm.js';
import {UserCardCardio, UserCardDentist, UserCardTerapevt} from './modules/userCard.js';
export const token = '40d2e985-3a13-4349-a746-2006e2dae4c6';
// const API = 'https://ajax.test-danit.com/api/json/';
// const form = document.querySelector('#post-form');
// const [formTitle, formBody] = document.querySelectorAll('[type="text"]');

const buttonAdd = document.querySelector('#btn-new-user');
const docsWrapper = document.querySelector('#docsWrapper');
export const cardDeck = document.querySelector(".card_deck");

export function renderCards(){
    cardDeck.innerHTML=''
    receiveCard(token)
    // .then(response=>console.log(response))
    .then(cards=>{
        cards.forEach(card=>{
            console.log(card)
            if(card.doctor==="Кардиолог"){
                const generateCard = new UserCardCardio(card)
                generateCard.renderCard(cardDeck)
            }
            if(card.doctor==="Терапевт"){
                const generateCard = new UserCardTerapevt(card)
                generateCard.renderCard(cardDeck)
            }
            if(card.doctor==="Стоматолог"){
                const generateCard = new UserCardDentist(card)
                generateCard.renderCard(cardDeck)
            }
        })
    })
}
renderCards()

buttonAdd.addEventListener('click', ()=>{
    const newForm = new UserForm();

    const newUserModal = new Modal ({
        headerTitle: 'Add new Appointment',
        body: newForm.render(cardDeck),
        closeOutside: true
    })

    document.body.append(newUserModal.render());
})
